\select@language {english}
\contentsline {chapter}{\numberline {1}Get TAPIS}{3}{chapter.1}
\contentsline {section}{\numberline {1.1}Required packages}{3}{section.1.1}
\contentsline {section}{\numberline {1.2}Download}{3}{section.1.2}
\contentsline {section}{\numberline {1.3}Install}{3}{section.1.3}
\contentsline {chapter}{\numberline {2}Tutorial}{5}{chapter.2}
\contentsline {section}{\numberline {2.1}Align reads}{5}{section.2.1}
\contentsline {section}{\numberline {2.2}Creating indexed, sorted BAM files}{5}{section.2.2}
\contentsline {section}{\numberline {2.3}Running \textbf {TAPIS}}{6}{section.2.3}
\contentsline {section}{\numberline {2.4}Interpreting \textbf {TAPIS} output}{6}{section.2.4}
\contentsline {chapter}{\numberline {3}Contact}{9}{chapter.3}
\contentsline {chapter}{Bibliography}{11}{chapter*.4}
